@extends('theme/base')

<body>
    @section('contingut')
    <div class="container py-5 text-center">
        <h1>Llistat d'productes</h1>
        <a href="{{ route('productes.create')}}" class="btn btn-primary">Crear Productes</a>
    </div>
    <table id="table" class="table">
        <thead class="thead-dark">
            <tr>
                <th style="cursor: pointer;" onclick="sortTable(0)" scope="col">Nom</th>
                <th style="cursor: pointer;" onclick="sortTable(1)" scope="col">Categoria</th>
                <th scope="col">Accions</th>
            </tr>
        </thead>
        <tbody>
            @forelse($productes as $producte)
            <tr>
                <td>{{ $producte->name }}</td>
                <td>{{ $producte->category}}</td>
                <td><a href="{{route('productes.edit', $producte)}}" class="btn btn-warning">Editar</a>
                    <form action="{{route('productes.destroy', $producte)}}" method="post" class="d-inline">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-danger" onclick="return confirm('¿Estas seguro?')">Eliminar</button>
                    </form>
                </td>
            </tr>
            @empty
            <td colspan="3">Actualmente no hi ha registres</td>
            @endforelse
        </tbody>
    </table>
    <div style="margin-left: 47%;">
        {{ $productes->links("pagination::bootstrap-4") }}
    </div>
    <script>
        function sortTable(n) {
            var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
            table = document.getElementById("table");
            switching = true;
            dir = "asc";
            while (switching) {
                switching = false;
                rows = table.rows;
                for (i = 1; i < (rows.length - 1); i++) {
                    shouldSwitch = false;
                    x = rows[i].getElementsByTagName("TD")[n];
                    y = rows[i + 1].getElementsByTagName("TD")[n];
                    if (dir == "asc") {
                        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                            shouldSwitch = true;
                            break;
                        }
                    } else if (dir == "desc") {
                        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                            shouldSwitch = true;
                            break;
                        }
                    }
                }
                if (shouldSwitch) {
                    rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                    switching = true;
                    switchcount++;
                } else {
                    if (switchcount == 0 && dir == "asc") {
                        dir = "desc";
                        switching = true;
                    }
                }
            }
        }
        // Referencia: https://www.w3schools.com/howto/howto_js_sort_table.asp
    </script>
    @stop
</body>