@extends('theme/base')
@section('contingut')
<div class="container py-5 text-center">

   @if (isset($producte))
   <h1>Editar producte</h1>
   @else
   <h1>Crear productes</h1>
   @endif

   @if (isset($producte))
   <form action="{{ route('productes.update', $producte)}}" method="post">
      @method('PUT')
   @else
   <form action="{{ route('productes.store')}}" method="post">
   @endif

   @csrf
   <div class="mb-3">
        <label for="name" class="form-label">Nom</label>
        <input type="text" name="name" class="form-control" placeholder="Nom de l'producte" value="{{old('name') ?? @$producte->name}}">
        @error('name')
           <p class="form-text text-danger">{{$message}}</p>
        @enderror
   </div>
   <div class="mb-3">
        <label for="category" class="form-label">Categoria</label>
        <select name="category" id="category" class="custom-select">
            <option value="Fruta" @if (old('category') == "Fruta") {{ 'selected' }} @endif>Fruta</option>
            <option value="Verdura" @if (old('category') == "Verdura") {{ 'selected' }} @endif >Verdura</option>
            <option value="Carne" @if (old('category') == "Carne") {{ 'selected' }} @endif>Carne</option>
            <option value="Pescado" @if (old('category') == "Pescado") {{ 'selected' }} @endif >Pescado</option>
        </select>
        <!-- <input type="text" name="category" class="form-control" placeholder="Categoria de l'producte" value="{{old('category') ?? @$producte->category}}" step="0,01"> -->
        @error('category')
           <p class="form-text text-danger">{{$message}}</p>
        @enderror
   </div>
   <div class="mb-3">
        <label for="description" class="form-label">Descripcion</label>
        <textarea type="text" name="description" class="form-control" placeholder="Descripcion del producte">{{old('description') ?? @$producte->description}}</textarea>
        @error('description')
           <p class="form-text text-danger">{{$message}}</p>
        @enderror  
   </div>

   @if (isset($producte))
   <button type="submit" class="btn btn-info">Editar producte</button>
   @else
   <button type="submit" class="btn btn-info">Desar producte</button>
   @endif
</form>
</div>
  
@endsection
