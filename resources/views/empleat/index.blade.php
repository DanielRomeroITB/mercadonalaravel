@extends('theme/base')
<body>
    @section('contingut')
    <div class="container py-5 text-center">
        <h1>Llistat d'empleats</h1>
        <a href="{{ route('empleats.create')}}" class="btn btn-primary">Crear Empleats</a>
    </div>
    <table class="table">
        <thead class="thead-dark">
            <tr>
            <th scope="col">Nom</th>
            <th scope="col">Sueldo</th>
            <th scope="col">Accions</th>
            </tr>
        </thead>
        <tbody>
            @forelse($empleats as $empleat)
            <tr>
                <td>{{ $empleat->name }}</td>
                <td>{{ $empleat->due}}</td>
                <td><a href="{{route('empleats.edit', $empleat)}}" class="btn btn-warning">Editar</a>
                    <form action="{{route('empleats.destroy', $empleat)}}" method="post" class="d-inline">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-danger" onclick="return confirm('¿Estas seguro?')">Eliminar</button>
                    </form>
                </td>
            </tr>
            @empty
                <td colspan="3">Actualmente no hi ha registres</td>
            @endforelse
        </tbody>
        </table>
        <div style="margin-left: 47%;">
            {{ $empleats->links("pagination::bootstrap-4") }}
        </div>
    @stop
</body>
