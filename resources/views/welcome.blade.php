@extends('theme/base')
<link rel="stylesheet" type="text/css" href="{{ asset('css/welcome.css') }}">

<body>
    @section('contingut')
    <div class="container">
        <div class="row">
            <div class="card col-3">
                <img class="card-img-top" src="https://info.mercadona.es/img-cont/es/gestion-del-talento-condiciones-trabajadores-de-mercadona.jpg">
                <div class="card-body">
                    <h5 class="card-title">Empleados</h5>
                    <p class="card-text">Gestion de empleados, modificacion de sueldos, despidos, nuevos empleados, etc...</p>
                    <a href="/empleats" class="btn btn-primary">Gestionar Empleados</a>
                </div>
            </div>
            <div class="offset-1"></div>
            <div class="card col-3">
                <img class="card-img-top" src="https://amp.65ymas.com/uploads/s1/35/09/39/tortitas-mercadona.jpeg">
                <div class="card-body">
                    <h5 class="card-title">Productos</h5>
                    <p class="card-text">Gestion de productos, precio, cantidad, posicionamiento en el supermercado, etc... <br><br></p>
                    <a href="/productes" class="btn btn-success">Gestionar Productos</a>
                </div>
            </div>
            <div class="offset-1"></div>
            <div class="card col-3">
                <img class="card-img-top" src="https://www.mercadona.com/estaticos/canal/img/mercadona_57_precios.jpg">
                <div class="card-body">
                    <h5 class="card-title">Ofertas</h5>
                    <p class="card-text">WIP Gestion de ofertas por producto, hasta que fecha, etc... <br><br><br></p>
                    <a href="#" class="btn btn-danger disabled">WIP</a>
                </div>
            </div>
        </div>
    </div>
    @stop
</body>