<?php

namespace App\Http\Controllers;

use App\Models\Empleat;
use Illuminate\Http\Request;

class EmpleatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empleats = Empleat::Paginate(5);
        return view('empleat.index')
            ->with('empleats', $empleats);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('empleat.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:15',
            'due' => 'required|gte:50'
        ]);

        $empleat = Empleat::create($request->only('name', 'due', 'comments'));

        return redirect()->route('empleats.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Empleat  $empleat
     * @return \Illuminate\Http\Response
     */
    public function show(Empleat $empleat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Empleat  $empleat
     * @return \Illuminate\Http\Response
     */
    public function edit(Empleat $empleat)
    {
        return view('empleat.form')
            ->with('empleat', $empleat);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Empleat  $empleat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Empleat $empleat)
    {
        $request->validate([
            'name' => 'required|max:15',
            'due' => 'required|gte:50'
        ]);

        $empleat->name = $request['name'];
        $empleat->due = $request['due'];
        $empleat->comments = $request['comments'];
        $empleat->save();

        return redirect()->route('empleats.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Empleat  $empleat
     * @return \Illuminate\Http\Response
     */
    public function destroy(Empleat $empleat)
    {
        $empleat->delete();
        return redirect()->route('empleats.index');
    }
}
